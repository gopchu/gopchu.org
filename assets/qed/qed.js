// *************************************************************************
// ** QED - The Cute Editor
// *************************************************************************
/*
(function ($) {
    $.fn.autogrow = function () {
        this.filter('textarea').each(function () {
            var $this = $(this),
                minHeight = $this.height(),
                shadow = $('<div></div>').css({
                    position:   'absolute',
                    top: -10000,
                    left: -10000,
                    width: $(this).width(),
                    fontSize: $this.css('fontSize'),
                    fontFamily: $this.css('fontFamily'),
                    lineHeight: $this.css('lineHeight'),
                    resize: 'none'
                }).addClass('shadow').appendTo(document.body),
                update = function () {
                    var t = this;
                    setTimeout(function () {
                        var val = t.value.replace(/</g, '&lt;')
                                .replace(/>/g, '&gt;')
                                .replace(/&/g, '&amp;')
                                .replace(/\n/g, '<br/>&nbsp;');

                        if ($.trim(val) === '') {
                            val = 'a';
                        }

                        shadow.html(val);
                        $(t).css('height', Math.max(shadow[0].offsetHeight + 20, minHeight));
                    }, 0);
                };
		console.log($(this).width());

            $this.change(update).keyup(update).keydown(update).focus(update);
            update.apply(this);
        });

        return this;
    };

}(jQuery));
*/

function resizeTextarea() {
    this.style.height = "";

    var
        $this = $(this),
        outerHeight = $this.outerHeight(),
        scrollHeight = this.scrollHeight,
        innerHeight = $this.innerHeight(),
        magic = outerHeight - innerHeight;
    this.style.height = scrollHeight + magic + "px";
    return true;
};

QED = function(options) {
    this.options = options;
    this.initialize();
};

QED.prototype.initialize = function() {
    var sourceTxt = this.options.source;

    this.converter = Markdown.getSanitizingConverter({
        allowComments: this.options.refmode?true: false,
    });

    //console.log(this.converter);

    var destHTML = this.converter.makeHtml( sourceTxt );

    this.options.editorarea.html(destHTML);
    
    if (typeof this.options.refmode == 'undefined') {
	this.makeClickEditable(this.options.editorarea.children());

	// need to ensure the last one is editable.
	var lastEditableElement = $('<p rel="lastEditable" class="last-editable"></p>');
	this.makeClickEditable(lastEditableElement);
	this.options.editorarea.append(lastEditableElement);
    } else {
	// ref mode
    }
    
    this.options.editorarea.addClass('qed-editor');

    this.populateDestination();
    
    parent = this;
}

QED.prototype.makeClickEditable = function(obj) {
    parent = this;

    /// this is the problemo
    // console.log(parentConverter.stash);
    
    obj.click(function() {
	//console.log(parentConverter.stash);
	parent.createInlineEditor( this, parent.converter );
    });
}

QED.prototype.createInlineEditor = function(obj, converter) {
    if (typeof converter == 'undefined')
	console.log("Converter is undefined!!!");
    //if (typeof obj.attr('rel') == 'undefined') return;

    // Need to resolve the active editor
    if (this.activeEditor)
	this.handleParagraphEditDone(this.activeEditor);
    
    var originalCode = '';
    var isLastElement = false;
    
    if (typeof $(obj).attr == 'function' &&
	typeof $(obj).attr('rel') != 'undefined') {
	    if ($(obj).attr('rel') == 'lastEditable')
		isLastElement = true;
	    originalCode = converter.getOriginalCodeByRelId($(obj).attr('rel'));
	    originalCode = originalCode.replace(/[\r\n]+$/, '');
	    originalCode = originalCode.replace(/^[\r\n]+/, '');
    }
    
    parent = this;
    
    var inlineEd = $(
	'<textarea></textarea>',
	{
	    value: originalCode,
	    /*
	    blur: function() {
		parent.handleParagraphEditDone(this);
	    },
	    */
	    /*
	    keyup: function(event) {
		parent.handleKeyUp(this, event.keyCode);
	    },
	    */
	    keydown: function(event) {
		return parent.handleKey(this, event.keyCode, converter);
	    },
	}
    );


    //$(inlineEd).autogrow();
     $(inlineEd)
 	.keydown(resizeTextarea);
 
     $(inlineEd)
 	.keyup(resizeTextarea);

    $(inlineEd)
	.change(resizeTextarea);
	
    $(inlineEd)
	.focus(resizeTextarea);
    
    if (!isLastElement) {
	$(obj).after(inlineEd);
	$(obj).remove();
    } else {
	$(obj).before(inlineEd);
    }

    $(inlineEd).focus();

    this.activeEditor = inlineEd;
}

QED.prototype.handleParagraphEditDone = function(obj) {
    var replacementEl = $(this.converter.makeHtml($(obj).val()));
    this.makeClickEditable(replacementEl);
    $(obj).before(replacementEl);
    $(obj).remove();

    this.activeEditor = null;
    if (typeof this.options.destination != 'null')
	this.populateDestination();
}

QED.prototype.handleKey = function(obj, keyCode, converter) {
    //console.log (keyCode);
    if (keyCode == 13) { // key enter
	var str = obj.value;
	if ((str.charAt(str.length-1) == '\n') /*&&
	    (str.charAt(str.length-2) == '\n')*/) {
	    var nextParagraph = $('<p></p>');
	    this.makeClickEditable(nextParagraph);
	    $(obj).after(nextParagraph);
	    this.createInlineEditor($(nextParagraph), this.converter);

	    return false;
	}
    } else if (keyCode == 27) { // key esc
	this.handleParagraphEditDone(obj);
	return false;
    } else if (keyCode == 38) { // key up
	if (obj.selectionStart == 0) {
	    //console.log('up');
	    this.createInlineEditor($(obj).prev(), this.converter);
	    return false;
	}
    } else if (keyCode == 40) { // key down
	if (obj.selectionStart == obj.value.length) {
	    //console.log('down');
	    this.createInlineEditor($(obj).next(), this.converter);
	    return false;
	}

    } else if (keyCode == 46) { // key del
	if (obj.selectionStart == obj.value.length) {
	    //console.log('down');
	    var oldCursorPos = obj.value.length;
	    var nextParagraph = $(obj).next();

	    if ($(nextParagraph).attr('rel') != 'lastEditable') {
		var nextPCode = converter.getOriginalCodeByRelId($(nextParagraph).attr('rel'));

		obj.value += " " + nextPCode;
	    
		this.setSelectionRange(obj, oldCursorPos, oldCursorPos);
	    
		$(nextParagraph).remove();
	    }
	    
	    return false;
	}
    } else if (keyCode == 8) { // key bkspace
	if (
	    (obj.selectionStart == obj.selectionEnd) &&
	    (obj.selectionStart == 0)
	) {
	    //console.log('bkspace');
	    var oldCursorPos = obj.value.length;
	    var prevParagraph = $(obj).prev();
	    var prevPCode = converter.getOriginalCodeByRelId($(prevParagraph).attr('rel'));

	    obj.value = prevPCode + " " + obj.value;

	    this.setSelectionRange(obj, prevPCode.length, prevPCode.length);

	    $(prevParagraph).remove();

	    return false;
	}
    } else {
	//console.log(keyCode);
	return true;
    }
    //else
	//console.log (str.charAt(str.length-1) + str.charAt(str.length-2));
}

QED.prototype.generateCode = function() {
    
    // Need to resolve the active editor
    if (this.activeEditor)
	this.handleParagraphEditDone(this.activeEditor);

    ret = '';

    parent =this;
    parentConverter =this.converter;
    
    this.options.editorarea.children().map(function() {
	if (typeof $(this).attr('rel') == 'undefined') return;
	ret += parentConverter.getOriginalCodeByRelId($(this).attr('rel')) + '\n\n';
    });

    return ret;
}

QED.prototype.populateDestination = function() {
    this.options.destination.val(this.generateCode());
}

QED.prototype.setSelectionRange = function(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

/*
	Simple OpenID Plugin
	http://code.google.com/p/openid-selector/
	
	This code is licensed under the New BSD License.
*/

var providers_large = {
	google : {
		name : 'Google',
		url : 'https://www.google.com/accounts/o8/id'
	},
	yahoo : {
		name : 'Yahoo',
		url : 'http://me.yahoo.com/'
	},
	aol : {
		name : 'AOL',
		label : 'Enter your AOL screenname.',
		url : 'http://openid.aol.com/{username}'
	},
	myopenid : {
		name : 'MyOpenID',
		label : 'Enter your MyOpenID username.',
		url : 'http://{username}.myopenid.com/'
	},
	openid : {
		name : 'OpenID',
		label : 'Enter your OpenID.',
		url : null
	}
};

var providers_small = {
	livejournal : {
		name : 'LiveJournal',
		label : 'Xin nhập vào tài khoản LiveJournal của bạn',
		url : 'http://{username}.livejournal.com/'
	},
	wordpress : {
		name : 'Wordpress',
		label : 'Xin nhập vào tài khoản Wordpress.com của bạn',
		url : 'http://{username}.wordpress.com/'
	},
	blogger : {
		name : 'Blogger',
		label : 'Xin nhập vào tài khoản Blogger của bạn',
		url : 'http://{username}.blogspot.com/'
	},
	verisign : {
		name : 'Verisign',
		label : 'Xin nhập vào tài khoản Verisign của bạn',
		url : 'http://{username}.pip.verisignlabs.com/'
	},
	flickr: {
		name: 'Flickr',
		label: 'Hãy nhập vào tài khoản Flickr của bạn',
		url: 'http://flickr.com/{username}/'
	},
	technorati: {
		name: 'Technorati',
		label: 'Xin nhập vào tài khoản Technorati của bạn',
		url: 'http://technorati.com/people/technorati/{username}/'
	},
// 	vidoop: {
// 		name: 'Vidoop',
// 		label: 'Your Vidoop username',
// 		url: 'http://{username}.myvidoop.com/'
// 	},
// 	launchpad: {
// 		name: 'Launchpad',
// 		label: 'Your Launchpad username',
// 		url: 'https://launchpad.net/~{username}'
// 	},
// 	claimid : {
// 		name : 'ClaimID',
// 		label : 'Your ClaimID username',
// 		url : 'http://claimid.com/{username}'
// 	},
// 	clickpass : {
// 		name : 'ClickPass',
// 		label : 'Enter your ClickPass username',
// 		url : 'http://clickpass.com/public/{username}'
// 	},
	google_profile : {
		name : 'Google Profile',
		label : 'Xin nhập vào tài khoản Google Profile của bạn',
		url : 'http://www.google.com/profiles/{username}'
	}
};

openid.locale = 'en';
openid.sprite = 'en'; // reused in german& japan localization
openid.demo_text = 'In client demo mode. Normally would have submitted OpenID:';
openid.signin_text = 'Đăng nhập';
openid.image_title = 'Đăng nhập với {provider}';

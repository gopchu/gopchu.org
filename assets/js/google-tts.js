/*! google-tts v1.0.0 | https://github.com/hiddentao/google-tts */
(function (name, definition){
  if ('function' === typeof define){ // AMD
    define(definition);
  } else if ('undefined' !== typeof module && module.exports) { // Node.js
    module.exports = definition();
  } else { // Browser
    var theModule = definition(), global = this, old = global[name];
    theModule.noConflict = function () {
      global[name] = old;
      return theModule;
    };
    global[name] = theModule;
  }
})('GoogleTTS', function () {
  /**
   * The API instance.
   *
   * @param defaultLanguage Optional. The language to use when not specified. 'en' is default.
   * @constructor
   */
  return function(defaultLanguage) {
    /**
     * Default language (code).
     * @type {String}
     */
    defaultLanguage = defaultLanguage || 'en';

    /**
     * Whether we can play MP3 audio or not. Gets set further down.
     * @type {Boolean}
     */
    var canPlayHTML5MP3Audio = undefined;

    /**
     * Full list of languages.
     * @type {Object}
     */
    var languages = {
      'af' : 'Afrikaans',
      'sq' : 'Albanian',
      'ar' : 'Arabic',
      'hy' : 'Armenian',
      'ca' : 'Catalan',
      'zh-CN' : 'Mandarin (simplified)',
      'zh-TW' : 'Mandarin (traditional)',
      'hr' : 'Croatian',
      'cs' : 'Czech',
      'da' : 'Danish',
      'nl' : 'Dutch',
      'en' : 'English',
      'eo' : 'Esperanto',
      'fi' : 'Finnish',
      'fr' : 'French',
      'de' : 'German',
      'el' : 'Greek',
      'ht' : 'Haitian Creole',
      'hi' : 'Hindi',
      'hu' : 'Hungarian',
      'is' : 'Icelandic',
      'id' : 'Indonesian',
      'it' : 'Italian',
      'ja' : 'Japanese',
      'ko' : 'Korean',
      'la' : 'Latin',
      'lv' : 'Latvian',
      'mk' : 'Macedonian',
      'no' : 'Norwegian',
      'pl' : 'Polish',
      'pt' : 'Portuguese',
      'ro' : 'Romanian',
      'ru' : 'Russian',
      'sr' : 'Serbian',
      'sk' : 'Slovak',
      'es' : 'Spanish',
      'sw' : 'Swahili',
      'sv' : 'Swedish',
      'ta' : 'Tamil',
      'th' : 'Thai',
      'tr' : 'Turkish',
      'vi' : 'Vietnamese',
      'cy' : 'Welsh'
   };

    /**
     * Get supported languages.
     * @return {Object} hashtable (language code -> description)
     */
    this.languages = function() {
      return languages;
    };

    /**
     * Construct the URL to fetch the speech audio for given text and language.
     * @param txt the text.
     * @param lang the language of the text. If omitted then default language is used.
     */
    this.url = function(txt, lang) {
      lang = lang || defaultLanguage;

      if (!txt || 0 >= txt.length)
        throw new Error('Need some text');

      return '/ui/tts.php?ie=UTF-8&tl=' + lang + '&q=' + txt;
    };

    /**
     * Fetch and play the speech audio for given text and language.
     * @param txt the text.
     * @param lang the language of the text. If omitted then default language is used.
     * @param cb Completion callback with signature (err).
     */
    this.play = function(txt, lang, cb) {
      // load the MP3
      var mySound = soundManager.createSound({
	    id: lang + ':' + txt,
	    url: this.url(txt, lang)
	    // onload: myOnloadHandler,
	    // other options here..
      });
      if (!mySound) {
      	alert('Sorry, your browser cannot play MP3 Audio. Please upgrade to a modern browser that supports HTML5, like Google Chrome, and/or whitelist this site if you have any flash blocker enabled with your current browser.');
      }
      
      mySound.play();
    };
  }
});

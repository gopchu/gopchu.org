<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print getTitle() ?> &raquo; <?php print $historyTitle ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="generator" content="WiGit" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="<?php print getCSSURL() ?>" />

</head>
<body>
<header role="banner">
<!-- BEGIN PAGE HEADER -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" id="homepage_link" href="/"><?php print getTitle() ?></a>
          <div class="nav-collapse">
	    <!-- main menu begins -->
		<ul class="nav" id="main_menu">
		    <li><a class="" href="<?php print getHomeURL() ?>">Wiki</a></li>
		</li>
		</ul>

            <!-- main menu ends -->
	  </div>
        </div>
      </div>
    </div>
<!-- END PAGE HEADER -->
</header>


<div class="container">
            <h1 id="title"><?php print $historyTitle ?></h1>

        <div id="history">
            <table class="table">
                <tr><th>Date</th><th>Author</th><th>Page</th><th>Message</th><th>View</th></tr>
            <?php
                foreach ($wikiHistory as $item) {
                    print "<tr>"
                        . "<td>" . $item["date"] . "</td>"
                        . "<td class='author'>" . $item["linked-author"] . "</td>"
                        . "<td class='page'><a href=\"" . getViewURL($item["page"]) . "\">" . $item["page"] . "</a></td>"
                        . "<td>" . $item["message"] . "</td>"
                        . "<td>" . "<a href=\"" . getViewURL($item["page"], $item["commit"]) . "\">View</a></td>"
                        . "<td>" . "</td>"
                        . "</tr>\n";
                }
            ?>
            </table>
        </div>
        
</div>

<!-- BEGIN PAGE FOOTER -->
<footer role="contentinfo">
    <div id="inner-footer" class="clearfix container">
	<ul class="menu">
	    <li><a href="#"><?php if (getUser() != "") { ?><?php print getUser(); } ?></a></li>
	</ul>
    </div>
</footer>


<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/google-code-prettify/prettify.js"></script>
<script src="/assets/js/bootstrap-dropdown.js"></script>
<script src="/assets/js/bootstrap-scrollspy.js"></script>
<script src="/assets/js/bootstrap-collapse.js"></script>


</body>
    <?php print getFooterTags() ?>
</html>

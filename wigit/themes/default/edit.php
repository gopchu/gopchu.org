<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print getTitle() ?> &raquo;<?php print getPage() ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="generator" content="WiGit" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="<?php print getCSSURL() ?>" />

</head>
<body>
<header role="banner">
<!-- BEGIN PAGE HEADER -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" id="homepage_link" href="/"><?php print getTitle() ?></a>
          <div class="nav-collapse">
	    <!-- main menu begins -->
		<ul class="nav" id="main_menu">
		    <li><a class="" href="<?php print getHomeURL() ?>">Wiki</a></li>
		</li>
		</ul>

            <!-- main menu ends -->
	  </div>
        </div>
      </div>
    </div>
<!-- END PAGE HEADER -->
</header>

<div class="container">

	<h1 id="title"><?php print getPage() ?></h1>
	<?php if(isset($REMOTE_REPO)) { ?>
	<p>[ <a href="<?php print getRemoteRepoPullURL()?>">pull latest changes from remote repo</a> ]</p>
	<?php } ?>

        <div id="form" class="wmd-panel">
            <form method="post" action="<?php print getPostURL(); ?>">
		<div id="wmd-button-bar"></div>
                <textarea name="data" cols="80" rows="20" style="width: 100%" class="wmd-input" id="wmd-input"><?php print getRawData(); ?></textarea>
		<a href="#" onclick="$('#wmd-preview').toggle();return false;">Hiện/giấu phần xem trước</a>
                <div id="wmd-preview" class="wmd-panel wmd-preview"></div>
                
                <div>Xin hãy nhập các chữ số sau vào khung <img src="imagebuilder.php"> <input maxlength=8 size=8 name="capuserstring" type="text" value=""/> <input type="submit" value="Đồng ý" /></div>
                <?php if(isset($REMOTE_REPO)) { ?>
                <p><input type=checkbox name="pushChanges" value="on">Push changes to remote repo?</p>
                <?php } ?>
            </form>
        </div>

</div>

<!-- BEGIN PAGE FOOTER -->
<footer role="contentinfo">
    <div id="inner-footer" class="clearfix container">
	<ul class="menu">
	    <li><a href="#"><?php if (getUser() != "") { ?><?php print getUser(); } ?></a></li>
	</ul>
    </div>
</footer>


<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/google-code-prettify/prettify.js"></script>
<script src="/assets/js/bootstrap-dropdown.js"></script>
<script src="/assets/js/bootstrap-scrollspy.js"></script>
<script src="/assets/js/bootstrap-collapse.js"></script>
<script src="/assets/pagedown/Markdown.Converter.js"></script>
<script src="/assets/pagedown/Markdown.Sanitizer.js"></script>
<script src="/assets/pagedown/Markdown.Editor.js"></script>
<script src="/assets/js/mudim-optimized.js"></script>
<script>
(function () {
                var converter1 = Markdown.getSanitizingConverter();
                var editor1 = new Markdown.Editor(converter1);
                editor1.run();
})();
</script>

</body>
    <?php print getFooterTags() ?>
</html>

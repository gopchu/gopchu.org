<?php
// Location of the Git binary
$GIT = "/usr/bin/git";

// Base URL of the wigit instalation. This will be prepended to all links
// to internal files.
$BASE_URL = "/ui";

// The script to which all requests should be passed. The default is
// $BASE_URL/index.php?r= , which should work on all installations. If you
// want to use pretty URLs, set this to $BASE_URL. (see README)
$SCRIPT_URL = "$BASE_URL/qed_ui.php?r=";
//$SCRIPT_URL = "$BASE_URL";

// Dir that contains the data files (i.e. the Git repository). This directory
// must be writable by the web server.
$DATA_DIR = "../../books";

// Remote Repo. If you wish to have the option to push the local wiki git store to have
// a remote origin and be able to push to it on edits enable the following.
//$REMOTE_REPO = "http://path.to/repo.git";

// The default author of the git commits.
$DEFAULT_AUTHOR = 'Anonymous <anonymous@gopchu.org>';
?>

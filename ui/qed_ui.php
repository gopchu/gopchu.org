<?php
require_once "config.php";
require_once "../models/session.php";
require_once "../models/projects.php";

require_once 'qed_config.php';
require_once 'qed_utils.php';

require_once 'Twig/Autoloader.php';

Twig_Autoloader::register();

$sess = new UserSession();

$projects = new Projects();
$projects->set_user($sess->get_oid());

/////////////////////////////////////////////////////////////////
// Get Project Info
$projectinfo = $projects->get(Array('uuid' => $_GET['project']));

if ($projectinfo == NULL) {
    die('Specified project UUID is not valid!');
}

$projectUUID = $projectinfo['uuid'];

$can_change = $projectinfo['can-edit'];

/////////////////////////////////////////////////////////////////
// Determine current tab

//SaveListingOrder
$current_tab = 'comment';

if (isset($_GET['tab']))
    $current_tab = $_GET['tab'];


$fn = false;

/////////////////////////////////////////////////////////////////
// Determine current section

if (isset($_GET['section']) && $_GET['section'] != '') {
    $fn = $_GET['section'];
} else {
    if ($current_tab == 'comment')
        $fn = 'intro.md';
}

/////////////////////////////////////////////////////////////////
// Prepare links

$tab_link = $_SERVER['SCRIPT_NAME'] . '?' .
    'project=' . $projectUUID . '&' .
    'section=' . $fn;

$section_link = $_SERVER['SCRIPT_NAME'] . '?' .
    'project=' . $projectUUID . '&' .
    'tab=' . $current_tab;


/////////////////////////////////////////////////////////////////
// Prepare project directory

$projectdir = $DATA_DIR . '/' . $projectUUID;

if (!dir_exists($projectUUID, $DATA_DIR . '/')) {
    mkdir($projectdir);
}

if (!file_exists($DATA_DIR . '/' . $projectUUID . '/' . 'project.config')) {
    touch($DATA_DIR . '/' . $projectUUID . '/' . 'project.config');
}

if (!file_exists($DATA_DIR . '/' . $projectUUID . '/' . 'intro.md')) {
    touch($DATA_DIR . '/' . $projectUUID . '/' . 'intro.md');
}

$fp = $projectdir . "/" . $fn;

if ($fn && !file_exists($fp)) {
    touch($fp);
}

/////////////////////////////////////////////////////////////////
// Process various actions

$act = isset($_GET['act']) ? $_GET['act'] : '';

if (($_SERVER['REQUEST_METHOD'] == 'POST') &&
    ($act == 'SaveFile') && $fn
) {

    // wants to save the file
    $fh = fopen($fp, 'w') or die("can't open file");
    fwrite($fh, $_POST['sauce']);
    fclose($fh);
    die('File saved successfully');

} else if (($_SERVER['REQUEST_METHOD'] == 'POST') &&
    ($act == 'SaveListingOrder')
) {
    $projectinfo['sections-order'] = json_decode($_POST['order']);
    $projects->update(Array('uuid' => $projectUUID), $projectinfo);
    //var_dump($projectinfo);
    die();
}

/////////////////////////////////////////////////////////////////
// Get directory listing

$dir_listing_entries = Array();
if ($handle = opendir($DATA_DIR . '/' . $projectUUID)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ($entry != '.' && $entry != '..') {
            if (pathinfo($entry, PATHINFO_EXTENSION) == 'md') {
                $dir_listing_entries[] = $entry;
            }
        }
    }
    closedir($handle);
}

$dir_listing_public = null;
$dir_listing_draft = null;

if (isset($projectinfo['sections-order'])) {
    $dir_listing_public = array_intersect($projectinfo['sections-order']['public'], $dir_listing_entries);
    $dir_listing_draft = array_intersect($projectinfo['sections-order']['draft'], $dir_listing_entries);
    $dir_listing_draft = $dir_listing_draft + array_diff($dir_listing_entries, array_merge($dir_listing_public, $dir_listing_draft));
} else {
    $dir_listing_draft = $dir_listing_entries;
    $dir_listing_public = Array();
}

/////////////////////////////////////////////////////////////////
// Prepare content of file

$data = '';
if (file_exists($fp) && $fn) {
    $handle = fopen($fp, 'r');
    $data = fread($handle, filesize($fp) + 1);
} else if ($current_tab != 'read' && $fn == false) {
    $fp = '../assets/welcome_letter.txt';
    $handle = fopen($fp, 'r');
    $data = fread($handle, filesize($fp));
}

/////////////////////////////////////////////////////////////////
// Render final result

$loader = new Twig_Loader_Filesystem('../templates');

$twig = new Twig_Environment($loader, array(
    //'cache' => '../../cache/templates',
));

$template = $twig->loadTemplate('qed_sxs.html');

$wikiUser = getHTTPUser();

echo $template->render(array(
    //'page_title' => 'Giao diện soạn sách',
    'account_name' => $sess->get_name(),
    'markdown_text' => $data,
    'dir_listing_public' => $dir_listing_public,
    'dir_listing_draft' => $dir_listing_draft,
    'current_tab' => $current_tab,
    'tab_link' => $tab_link,
    'section_link' => $section_link,
    'current_section' => $fn,
    'can_change' => $can_change,
));

?>
<?php
/**
 * Part of the Gopchu project
 * User: wasabi
 * Date: 5/16/12
 * Time: 9:33 PM
 * Displays project information.
 */

require_once "config.php";
require_once "utils.php";
require_once "../models/session.php";
require_once "../models/projects.php";

$sess = new UserSession();


// Fetch project information from database
$projects = new Projects();
$projects->set_user($sess->get_oid());

$projectdata = $projects->get(Array('uuid' => $_GET['project']));

$projectdata['project-owners'] = implode(',', $projectdata['project-owners']);

require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('../templates');

$twig = new Twig_Environment($loader, array(
    //'cache' => '../../cache/templates',
));

$template = $twig->loadTemplate('project_info.html');

echo $template->render(array(
    //'page_title' => '',
    'account_name' => $sess->get_name(),
    'project_info' => $projectdata
));


?>
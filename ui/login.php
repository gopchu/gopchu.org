<?php
require_once "config.php";
require_once "../models/session.php";

$sess = new UserSession();

if (isset($_GET['act']) && $_GET['act'] == 'log_out') {

    // log out
    $sess->log_out();
    header("location: ${UI_ROOT}/#loggedout");

} else {

    require_once 'Twig/Autoloader.php';
    Twig_Autoloader::register();

    $loader = new Twig_Loader_Filesystem('../templates');

    $twig = new Twig_Environment($loader, array(
        //'cache' => '../../cache/templates',
    ));

    $template = $twig->loadTemplate('login.html');

    echo $template->render(array(
        'page_title' => 'Trang chủ',
        'account_name' => $sess->get_name()
    ));

}
?>

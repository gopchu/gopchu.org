<?php
require_once "config.php";
require_once "utils.php";
require_once "../models/session.php";
require_once "../models/projects.php";

require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('../templates');

$twig = new Twig_Environment($loader, array(
    //'cache' => '../../cache/templates',
));

$template = $twig->loadTemplate('project_list.html');

$sess = new UserSession();

$projects = new Projects();

$projects->set_user($sess->get_oid());

$project_list = null;

if (!isset($_GET['constrains'])) {
    $project_list = $projects->get_list();
} else if ($_GET['constrains'] == 'mine') {
    $sess->assert_loggedin();
    $project_list = $projects->get_list(
        array(
            '$or' =>
            array(
                array('owner' => $sess->get_oid()),
                array('project-owners' => $sess->get_email())
            )
        )
    );
}

echo $template->render(array(
    //'page_title' => '',
    'account_name' => $sess->get_name(),
    'projects' => $project_list
));
?>

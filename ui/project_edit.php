<?php
require_once "config.php";
require_once "utils.php";
require_once "../models/session.php";
require_once "../models/projects.php";

$sess = new UserSession();

$sess->assert_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //var_dump($_POST);

    $projects = new Projects();
    $projects->set_user($sess->get_oid());
    $final_filename = false;

    if (isset($_FILES['cover-file']['name']) && $_FILES['cover-file']['name'] != '') {
        // Process cover
        $temp_file_large = tempnam(sys_get_temp_dir(), 'upload_thumb');

        $ext = strtolower(pathinfo($_FILES['cover-file']['name'], PATHINFO_EXTENSION));

        gd_resize_crop($ext, $_FILES['cover-file']['tmp_name'], $temp_file_large, 240, 360, 80, 0, 1);

        $final_filename = 'covers/' . $uuid . '.' . $ext;

        $projects->store_file($temp_file_large, $final_filename);
        // Done processing cover
    }

    $projectdata = array();

    if ($_GET['act'] == 'register') {
        $uuid = gen_uuid();
        $projectdata['uuid'] = $uuid;
        $projectdata['owner'] = $sess->get_oid();
    } else if ($_GET['act'] == 'edit') {
        $projectdata = $projects->get(Array('uuid' => $_GET['project']));
        if (!$projectdata['can-edit']) die('You cannnot edit this project... :-(');
    }

    array_selective_copy($_POST, $projectdata, array(
            'project-name',
            'book-name-original',
            'project-intro-short',
            'project-intro',
            'project-is-translation-work',
            'project-is-draft',
            'project-owners',
            'book-translator')
    );

    if ($final_filename)
        $projectdata['cover-img'] = $final_filename;

    $projectdata['project-owners'] = explode(',', $projectdata['project-owners']);

    if ($_GET['act'] == 'register') {
        $projects->insert($projectdata);
    } else if ($_GET['act'] == 'edit') {
        $projects->update(Array('uuid' => $_GET['project']), $projectdata);
    }

    header('location: project_info.php?project=' . $projectdata['uuid']);
} else {

    // Fetch project information from database
    $projects = new Projects();
    $projects->set_user($sess->get_oid());

    $projectdata = null;
    if ($_GET['act'] == 'edit') {
        $projectdata = $projects->get(Array('uuid' => $_GET['project']));

        $projectdata['project-owners'] = implode(',', $projectdata['project-owners']);
    }

    require_once 'Twig/Autoloader.php';
    Twig_Autoloader::register();

    $loader = new Twig_Loader_Filesystem('../templates');

    $twig = new Twig_Environment($loader, array(
        //'cache' => '../../cache/templates',
    ));

    $template = $twig->loadTemplate('project_edit_form.html');

    echo $template->render(array(
        //'page_title' => '',
        'account_name' => $sess->get_name(),
        'project_info' => $projectdata
    ));
}
?>

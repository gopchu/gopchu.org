<?php
    require_once "config.php";
    $cachefn = $_GET['tl'] . '_' . md5($_GET['q']) . '.mp3';

    if (!file_exists($CACHE_DIR . '/tts/' . $cachefn)) {

        $fp = fopen ($CACHE_DIR . '/tts/' . $cachefn, 'w+');//This is the file where we save the information
        $ch = curl_init();

        //Here is the file we are downloading
        curl_setopt($ch, CURLOPT_URL, 'http://translate.google.com/translate_tts?' . $_SERVER["QUERY_STRING"]);

        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    header('location: ' . $CACHE_PATH . '/tts/' . $cachefn);
?>
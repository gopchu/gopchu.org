<?php
require_once 'config.php';

require_once "auth_common.php";
require_once "../models/session.php";

function escape($thing)
{
    return htmlentities($thing);
}

function run()
{
    $consumer = getConsumer();

    // Complete the authentication process using the server's
    // response.
    $return_to = getReturnTo();
    $response = $consumer->complete($return_to);

    // Check the response status.
    if ($response->status == Auth_OpenID_CANCEL) {
        // This means the authentication was cancelled.
        //$msg = 'Verification cancelled.';
        header("location: $UI_ROOT/#logincancelled");
    } else if ($response->status == Auth_OpenID_FAILURE) {
        // Authentication failed; display the error message.
        //$msg = "OpenID authentication failed: " . $response->message;
        die("OpenID authentication failed: " . $response->message);
        header("location: $UI_ROOT/#loginfailed");
    } else if ($response->status == Auth_OpenID_SUCCESS) {
        // This means the authentication succeeded; extract the
        // identity URL and Simple Registration data (if it was
        // returned).
        $openid = $response->getDisplayIdentifier();
        /*
        $esc_identity = escape($openid);

        $success = sprintf('You have successfully verified ' .
                           '<a href="%s">%s</a> as your identity.',
                           $esc_identity, $esc_identity);
	*/
        /*
        if ($response->endpoint->canonicalID) {
            $escaped_canonicalID = escape($response->endpoint->canonicalID);
            $success .= '  (XRI CanonicalID: '.$escaped_canonicalID.') ';
        }
        */

        $sreg_resp = Auth_OpenID_SRegResponse::fromSuccessResponse($response);

        $sreg = $sreg_resp->contents();

        $reg_fullname = 'Anonymous Coward';
        $reg_email = '';
        $reg_nickname = 'anonymous';

        if (@$sreg['email']) {
            $reg_email = $sreg['email'];
        }

        if (@$sreg['nickname']) {
            $reg_nickname = $sreg['nickname'];
        }

        if (@$sreg['fullname']) {
            $reg_fullname = $sreg['fullname'];
        }

        $ax = new Auth_OpenID_AX_FetchResponse();
        $obj = $ax->fromSuccessResponse($response);

        // Print me raw
        /*
      $success .= "<p>Extra info babe:</p>";
      $success .= '<pre>';
      $success .= print_r($obj->data, true);
      $success .= '</pre>';
      */

        if (@$obj->data && @$obj->data['http://axschema.org/contact/email']) {
            $reg_email = $obj->data['http://axschema.org/contact/email'][0];
        }

        if (@$obj->data &&
            @$obj->data['http://axschema.org/contact/first'] &&
                @$obj->data['http://axschema.org/contact/last']
        ) {
            $reg_fullname = $obj->data['http://axschema.org/contact/last'][0] .
                ' ' . $obj->data['http://axschema.org/contact/first'][0];
        }

        $sess = new UserSession();
        $sess->oid($openid);

        if (!($sess->is_registered())) {
            $sess->register_new_user($reg_email, $reg_nickname, $reg_fullname);
            header("location: $UI_ROOT/#newaccount");
        } else {
            header("location: $UI_ROOT/#loggedin");
        }
    }


}

run();

?>
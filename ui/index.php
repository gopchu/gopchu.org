<?php
require_once "config.php";
require_once "../models/session.php";

require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('../templates');

$twig = new Twig_Environment($loader, array(
    //'cache' => '../../cache/templates',
));

$template = $twig->loadTemplate('index_landing.html');

$sess = new UserSession();

echo $template->render(array(
    //'page_title' => 'Giao diện soạn sách',
    'account_name' => $sess->get_name()
));

?>

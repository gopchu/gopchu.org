<?php

/* Selectively copies elements from $source_array */

function array_selective_copy($source_array, &$dest_array, $elements) {
    foreach ($elements as $value) {
        if (isset($source_array[$value]))
            $dest_array[$value] = $source_array[$value];
    }
    return $dest_array;
}

function gen_uuid()
{
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}

/*Begin - Resize and Crop image with GD2*/
function gd_resize_crop($ext, $source, $target, $width, $height, $quality, $resize, $center)
{
    //$ext = strtolower(pathinfo($source, PATHINFO_EXTENSION));
    list($width_orig, $height_orig) = getimagesize($source);

    if ($center == 1) { ///Crop centre
        $image_p = imagecreatetruecolor($width, $height);
        $width_temp = $width;
        $height_temp = $height;

        if ($width_orig / $height_orig > $width / $height) {
            $width = $width_orig * $height / $height_orig;
            $x_pos = -($width - $width_temp) / 2;
            $y_pos = 0;
        } else {
            $height = $height_orig * $width / $width_orig;
            $y_pos = -($height - $height_temp) / 2;
            $x_pos = 0;
        }

    } else { ///Just resize
        if ($resize == 0) {
        } ///Resize to exact new width&height

        if ($resize == 1) { ///Resize to these max width&height
            if ($width_orig < $width && $height_orig < $height) {
                $width = $width_orig;
                $height = $height_orig;
            } else {
                if ($width_orig / $height_orig > $width / $height) {
                    $height = $width * $height_orig / $width_orig;
                } else {
                    $width = $height * $width_orig / $height_orig;
                }
            }
        }

        if ($resize == 2) {
            if ($width_orig > $width) $height = $height_orig * $width / $width_orig; else {
                $width = $width_orig;
                $height = $height_orig;
            }
        } ///Dynamic Height

        if ($resize == 3) {
            if ($height_orig > $height) $width = $width_orig * $height / $height_orig; else {
                $width = $width_orig;
                $height = $height_orig;
            }
        } ///Dynamic Width

        $image_p = imagecreatetruecolor($width, $height); //

    }

    if ($ext == "jpg" || $ext == "jpeg") $image = imagecreatefromjpeg($source);
    else if ($ext == "png") {
        imagealphablending($image_p, false);
        imagesavealpha($image_p, true);
        $image = imagecreatefrompng($source);
    }
    //convert transparent

    if ($center == 1) imagecopyresampled($image_p, $image, $x_pos, $y_pos, 0, 0, $width, $height, $width_orig, $height_orig);
    else imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

    if ($ext == "jpg" || $ext == "jpeg") imagejpeg($image_p, $target, $quality);
    if ($ext == "png") imagepng($image_p, $target, 9);

}

?>
<?php
/************************************************************************
 * Gop chu project, session model
 * (c) 2012 Huan Truong <htruong@tnhh.net>
 * THIS IS NOT FREE SOFTWARE.
 ************************************************************************/

include_once 'users.php';

session_start();

class UserSession
{
    protected $user;
    protected $oid;

    function __construct()
    {
        global $_SESSION;
        if (isset($_SESSION['oid'])) {
            $this->user = $_SESSION['user'];
            $this->oid = $_SESSION['oid'];
        }
    }

    function oid($oid)
    {
        $users = new Users();

        $this->oid = $oid;
        $this->user = $users->get(array('oid' => $oid));

        $_SESSION['oid'] = $oid;
        $_SESSION['user'] = $this->user;

        return $this->user;
    }

    function get_oid()
    {
        return $this->oid;
    }

    function get_email()
    {
        if ($this->is_registered()) {
            return $_SESSION['user']['email'];
        } else {
            return 'anonymous@gopchu.org';
        }
    }

    function assert_loggedin()
    {
        if (!$this->is_registered()) {
            header('location: login.php');
            die();
        }
    }

    function is_registered()
    {
        return (isset($this->user) && $this->user != NULL);
    }

    function register_new_user($email = '', $nickname = '', $fullname = '')
    {
        if (!isset($this->oid)) return;
        $users = new Users();

        $this->user = $users->insert(array(
            'oid' => $this->oid,
            'email' => $email,
            'nickname' => $nickname,
            'fullname' => $fullname,
        ));

        return $this->user;
    }

    function get_name()
    {
        if (isset($_SESSION['user'])) {
// 	    if ($_SESSION['user']['nickname']) {
// 		return $_SESSION['user']['nickname'];
// 	    } else {
            return $_SESSION['user']['email'];
// 	    }
        } else {
            return null;
        }
    }

    function log_out()
    {
        $_SESSION['oid'] = null;
        $_SESSION['user'] = null;
    }

    function get_permissions()
    {
        return $this->user['permissions'];
    }

    function get_records()
    {
        return $this->user;
    }

    function update_records($data)
    {
        if ($_SESSION['oid']) {
            $users = new Users();
            $users->update(array('oid' => $_SESSION['oid']), $data);
            return true;
        } else {
            return false;
        }
    }
}

?>
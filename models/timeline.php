<?php
/************************************************************************
 * Gop chu project, timeline model
 * -------------------------------
 * The timeline is responsible to events of an object
 * For example, a person's post on a project's wall.
 * Or a change in the project...
 * Struct timeline_event {
 *  actor => the actor
 *  dest => destination of the action
 *  metadata => {
 *      - all metadata needed for action -
 *  }
 *
 * (c) 2012 Huan Truong <htruong@tnhh.net>
 * THIS IS ALMOST FREE SOFTWARE.
 ************************************************************************/

define('EVENT_TYPE_POST', 100);
define('EVENT_TYPE_COMMENT', 110);

define('EVENT_TYPE_NEW_COMMIT', 200);

define('EVENT_TYPE_NEW_CHAPTER', 210);
define('EVENT_TYPE_NEW_PUBLIC_CHAPTER', 220);



?>
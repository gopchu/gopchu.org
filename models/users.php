<?php
/************************************************************************
 * Gop chu project, users model
 * (c) 2012 Huan Truong <htruong@tnhh.net>
 * THIS IS NOT FREE SOFTWARE.
 ************************************************************************/

class Users
{

    protected $collection;

    function __construct()
    {
        $connection = new Mongo();
        $this->collection = $connection->gopchu->users;
    }

    function get_list($constrains = null)
    {
        $cursor = $this->collection->find($constrains);

        $ret = array();

        foreach ($cursor as $result) {
            $ret = $result;
        }

        return $ret;
    }

    function get($constrains = null)
    {
        return $this->collection->findOne($constrains);
    }

    function insert($record)
    {
        return $this->collection->insert($record);
    }

    function update($constrains, $record)
    {
        $ret = $this->collection->findOne($constrains);
        if ($ret) {
            return $this->collection->update($constrains, $record);
        }
    }
}

?>
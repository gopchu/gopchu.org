<?php
/************************************************************************
 * Gop chu project, projects model
 * (c) 2012 Huan Truong <htruong@tnhh.net>
 * THIS IS NOT FREE SOFTWARE.
 ************************************************************************/

class Projects
{

    private $collection;

    function __construct()
    {
        $connection = new Mongo();
        $this->collection = $connection->gopchu->projects;
    }

    function get_list($constrains = null)
    {
        $cursor = null;

        if (!isset($constrains)) {
            $cursor = $this->collection->find();
        } else {
            $cursor = $this->collection->find($constrains);
        }

        $ret = Array();

        foreach ($cursor as $obj) {
            // TODO: Determine if the user can read the book or not. They always can now.
            $obj['can-edit'] = $this->can_edit($obj);
            $ret['can-read'] = true;
            $ret[] = $obj;
        }

        return $ret;
    }

    function set_user($oid)
    {
        $this->oid = $oid;
    }

    function can_edit($obj)
    {
        // TODO: Kind of ugly
        $can_edit = false;
        $is_one_of_owners = false;

        if (isset($_SESSION['user']['email']) && isset($obj['project-owners'])) {
            if (gettype($obj['project-owners']) == 'array')
                $is_one_of_owners = in_array($_SESSION['user']['email'], $obj['project-owners']);
            //else
            //    $is_one_of_owners = true;
        }

        if (isset($this->oid) && (($obj['owner'] == $this->oid) || $is_one_of_owners)) {
            $can_edit = true;
        }

        return $can_edit;
    }

    function get($constrains = null)
    {
        $ret = $this->collection->findOne($constrains);

        $ret['can-edit'] = $this->can_edit($ret);
        $ret['can-read'] = true;

        return $ret;
    }

    function insert($record)
    {
        // filter the 'can-edit' variable we might have injected when we returned the record to the user
        if (isset($record['can-edit']))
            unset($record['can-edit']);
        if (isset($record['can-read']))
            unset($record['can-read']);

        return $this->collection->insert($record);
    }

    function update($constrains, $record)
    {
        // filter the 'can-edit' variable we might have injected when we returned the record to the user
        if (isset($record['can-edit']))
            unset($record['can-edit']);
        if (isset($record['can-read']))
            unset($record['can-read']);

        $ret = $this->collection->findOne($constrains);
        if ($ret) {
            return $this->collection->update($constrains, $record);
        }
    }

    function store_file($file_to_store, $filename)
    {
        $mongo = new Mongo();
        $db = $mongo->assets;

        $grid = $db->getGridFS();

        $storedfile = $grid->storeFile(
            $file_to_store,
            array('filename' => $filename),
            array('safe' => true)
        );
    }
}